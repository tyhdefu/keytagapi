package com.tyhdefu.keytag.api;

import com.google.inject.Inject;

public final class KeyTag {

    @Inject
    private static Plugin plugin;

    public static Plugin plugin() {
        if (KeyTag.plugin == null) {
            throw new IllegalStateException("KeyTag has not been initialized!");
        }
        return KeyTag.plugin;
    }
}
