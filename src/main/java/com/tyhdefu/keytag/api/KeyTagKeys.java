package com.tyhdefu.keytag.api;

import com.tyhdefu.keytag.api.tree.TreeType;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.data.Key;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.data.value.Value;

public class KeyTagKeys {
    public static final Key<Value<DyeColor>> DYE_COLOR = of("dye_color", DyeColor.class);

    public static final Key<Value<TreeType>> TREE_TYPE = of("tree_type", TreeType.class);

    private static <T> Key<Value<T>> of(final String id, final Class<T> type) {
        return Key.builder().key(ResourceKey.of(KeyTagConstants.PLUGIN_ID, id)).elementType(type).build();
    }
}
