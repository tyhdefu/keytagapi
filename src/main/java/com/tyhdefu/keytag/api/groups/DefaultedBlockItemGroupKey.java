package com.tyhdefu.keytag.api.groups;

import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.api.registry.RegistryHolder;
import org.spongepowered.api.registry.RegistryKey;

import java.util.function.Supplier;

public final class DefaultedBlockItemGroupKey<T> {

    private final DefaultedRegistryReference<Group<BlockType, T>> block;
    private final DefaultedRegistryReference<Group<ItemType, T>> item;

    public static <T> DefaultedBlockItemGroupKey<T> of(final ResourceKey key, Supplier<RegistryHolder> defaultHolder) {
        return new DefaultedBlockItemGroupKey<>(
                RegistryKey.of(KeyTagRegistryTypes.BLOCK_TYPE_GROUPS, key).asDefaultedReference(defaultHolder),
                RegistryKey.of(KeyTagRegistryTypes.ITEM_TYPE_GROUPS, key).asDefaultedReference(defaultHolder)
        );
    }

    private DefaultedBlockItemGroupKey(DefaultedRegistryReference<Group<BlockType, T>> block,
                                       DefaultedRegistryReference<Group<ItemType, T>> item) {
        this.block = block;
        this.item = item;
    }

    public DefaultedRegistryReference<Group<ItemType, T>> item() {
        return this.item;
    }

    public DefaultedRegistryReference<Group<BlockType, T>> block() {
        return this.block;
    }
}
