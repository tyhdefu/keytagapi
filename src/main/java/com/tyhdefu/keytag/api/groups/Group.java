package com.tyhdefu.keytag.api.groups;

import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.api.registry.DefaultedRegistryValue;
import org.spongepowered.api.registry.RegistryKey;
import org.spongepowered.api.registry.RegistryType;
import org.spongepowered.plugin.PluginContainer;

import java.util.Collection;
import java.util.Optional;
import java.util.function.Supplier;

public interface Group<H extends DataHolder, V> extends DefaultedRegistryValue {

    /**
     * Gets the {@link ResourceKey} id of this group.
     *
     * <p>Only guaranteed to be unique for groups
     * with the same {@link #registryType()}.</p>
     *
     * @return the id
     */
    ResourceKey id();

    /**
     * Gets the registry that this group
     * is contained in.
     *
     * @return The {@link RegistryType}
     */
    RegistryType<Group<H, ?>> registryType();

    /**
     * Checks whether this group contains
     * the given holder.
     * @param holder Holder to check for.
     * @return If the holder is contained in this group
     */
    boolean contains(H holder);

    /**
     * Gets the value for the given holder,
     * requiring it to be present. If it isn't
     * an exception will be thrown.
     *
     * @param holder Holder to get value for.
     * @return The associated value
     */
    V require(H holder);

    /**
     * Gets the value for the given holder,
     * returning an empty optional if not present.
     *
     * @param holder Holder to get value for.
     * @return The value, or an empty optional if not present.
     */
    Optional<V> get(H holder);

    /**
     * Gets the holder for the given value,
     * requiring it to be present. If it isn't
     * and exception will be thrown.
     *
     * @param value Value to get holder for.
     * @return The associated value.
     */
    H requireHolderFor(V value);

    /**
     * Gets the holder for the given value,
     * returning an empty optional if not present.
     *
     * @param value Value to get holder for.
     * @return The holder if present, or an empty optional if not present.
     */
    Optional<H> holderFor(V value);

    /**
     * Gets all holders contained in this group.
     *
     * @return All holders in this group
     */
    Collection<H> allHolders();

    /**
     * <p>Registers an additional entry in this group.
     * The PluginContainer provided will be used to log
     * who caused an registration.</p>
     *
     * <p><b>This should only be used for mods</b>
     * (Preferably that you own).
     * Registering vanilla holders here is incorrect, and
     * may cause unexpected behaviour by other plugins.</p>
     *
     * <p>This method takes {@link DefaultedRegistryReference} so that
     * it can also obtain the ResourceKey as well as the value,
     * and guarantee that they are matching. If you don't have
     * one you can create one with {@link RegistryKey#of(RegistryType, ResourceKey)}
     * then make it into a defaulted reference with {@link RegistryKey#asDefaultedReference(Supplier)}</p>
     *
     * @param cause The cause the additional registration.
     * @param holder Holder to register
     * @param value Value to register
     */
    void registerAdditional(PluginContainer cause, DefaultedRegistryReference<H> holder, DefaultedRegistryReference<V> value);
}
