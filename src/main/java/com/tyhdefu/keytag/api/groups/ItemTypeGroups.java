package com.tyhdefu.keytag.api.groups;

import com.tyhdefu.keytag.api.KeyTagConstants;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.api.registry.RegistryKey;
import org.spongepowered.api.registry.RegistryScope;
import org.spongepowered.api.registry.RegistryScopes;

@RegistryScopes(scopes = RegistryScope.GAME)
public final class ItemTypeGroups {

    public static final DefaultedRegistryReference<Group<ItemType, DyeColor>> DYE = of("dye");

    public static <T> DefaultedRegistryReference<Group<ItemType, T>> of(final String id) {
        return RegistryKey.of(KeyTagRegistryTypes.ITEM_TYPE_GROUPS, ResourceKey.of(KeyTagConstants.PLUGIN_ID, id))
                .asDefaultedReference(() -> Sponge.game().registries());
    }

}
