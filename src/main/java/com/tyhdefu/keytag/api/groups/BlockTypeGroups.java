package com.tyhdefu.keytag.api.groups;

import com.tyhdefu.keytag.api.KeyTagConstants;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import com.tyhdefu.keytag.api.tree.TreeType;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.data.type.DyeColor;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.api.registry.RegistryKey;
import org.spongepowered.api.registry.RegistryScope;
import org.spongepowered.api.registry.RegistryScopes;

@RegistryScopes(scopes = RegistryScope.GAME)
public final class BlockTypeGroups {

    public static final DefaultedRegistryReference<Group<BlockType, DyeColor>> POTTED_TULIP = of("potted_tulip");

    public static final DefaultedRegistryReference<Group<BlockType, TreeType>> POTTED_SAPLING = of("potted_sapling");

    public static final DefaultedRegistryReference<Group<BlockType, DyeColor>> WALL_BANNER = of("wall_banner");

    public static final DefaultedRegistryReference<Group<BlockType, TreeType>> WALL_SIGN = of("wall_sign");

    public static <T> DefaultedRegistryReference<Group<BlockType, T>> of(final String id) {
        return RegistryKey.of(KeyTagRegistryTypes.BLOCK_TYPE_GROUPS, ResourceKey.of(KeyTagConstants.PLUGIN_ID, id))
                .asDefaultedReference(() -> Sponge.game().registries());
    }
}
