package com.tyhdefu.keytag.api;

import com.tyhdefu.keytag.api.groups.Group;
import com.tyhdefu.keytag.api.tree.TreeType;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.block.BlockType;
import org.spongepowered.api.data.DataHolder;
import org.spongepowered.api.item.ItemType;
import org.spongepowered.api.registry.DefaultedRegistryType;
import org.spongepowered.api.registry.RegistryRoots;
import org.spongepowered.api.registry.RegistryType;

public class KeyTagRegistryTypes {
    public static final DefaultedRegistryType<Group<BlockType, ?>> BLOCK_TYPE_GROUPS = groupInGame("block_type_group");

    public static final DefaultedRegistryType<Group<ItemType, ?>> ITEM_TYPE_GROUPS = groupInGame("item_type_group");

    public static final DefaultedRegistryType<TreeType> TREE_TYPE = inGame("tree_type");

    public static <T> DefaultedRegistryType<T> inGame(final String id) {
        return RegistryType.of(RegistryRoots.SPONGE, ResourceKey.of(KeyTagConstants.PLUGIN_ID, id)).asDefaultedType(() -> Sponge.game().registries());
    }

    public static <T extends DataHolder> DefaultedRegistryType<Group<T, ?>> groupInGame(final String id) {
        return RegistryType.of(RegistryRoots.SPONGE, ResourceKey.of(KeyTagConstants.PLUGIN_ID, id)).asDefaultedType(() -> Sponge.game().registries());
    }
}
