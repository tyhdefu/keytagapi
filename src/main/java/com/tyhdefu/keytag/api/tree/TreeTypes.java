package com.tyhdefu.keytag.api.tree;

import com.tyhdefu.keytag.api.KeyTagConstants;
import com.tyhdefu.keytag.api.KeyTagRegistryTypes;
import org.spongepowered.api.ResourceKey;
import org.spongepowered.api.Sponge;
import org.spongepowered.api.registry.DefaultedRegistryReference;
import org.spongepowered.api.registry.RegistryKey;
import org.spongepowered.api.registry.RegistryScope;
import org.spongepowered.api.registry.RegistryScopes;

@RegistryScopes(scopes = RegistryScope.GAME)
public final class TreeTypes {

    public static final DefaultedRegistryReference<TreeType> ACACIA = of("acacia");

    public static final DefaultedRegistryReference<TreeType> BIRCH = of("birch");

    public static final DefaultedRegistryReference<TreeType> CRIMSON = of("crimson");

    public static final DefaultedRegistryReference<TreeType> DARK_OAK = of("dark_oak");

    public static final DefaultedRegistryReference<TreeType> JUNGLE = of("jungle");

    public static final DefaultedRegistryReference<TreeType> OAK = of("oak");

    public static final DefaultedRegistryReference<TreeType> SPRUCE = of("spruce");

    public static final DefaultedRegistryReference<TreeType> WARPED = of("warped");

    private static DefaultedRegistryReference<TreeType> of(final String id) {
        return RegistryKey.<TreeType>of(KeyTagRegistryTypes.TREE_TYPE, ResourceKey.of(KeyTagConstants.PLUGIN_ID, id))
                .asDefaultedReference(() -> Sponge.game().registries());
    }
}
