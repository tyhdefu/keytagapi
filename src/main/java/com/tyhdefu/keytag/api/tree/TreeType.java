package com.tyhdefu.keytag.api.tree;

import org.spongepowered.api.registry.DefaultedRegistryValue;
import org.spongepowered.api.util.annotation.CatalogedBy;

@CatalogedBy(TreeTypes.class)
public interface TreeType extends DefaultedRegistryValue {

}
