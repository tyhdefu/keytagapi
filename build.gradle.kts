plugins {
    `java-library`
}

repositories {
    mavenCentral()
    maven {
        name = "sponge"
        url = uri("https://repo.spongepowered.org/repository/maven-public/")
    }
}

dependencies {
    api("org.spongepowered:spongeapi:8.0.0-SNAPSHOT")
}